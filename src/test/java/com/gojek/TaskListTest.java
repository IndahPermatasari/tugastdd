package com.gojek;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskListTest {
    @Test
    public void testGetAllTask() {
        TaskList taskList = new TaskList();
        String expectedTask = "1. Do dishes [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]";

        taskList.addTask("1", "Do dishes", true, "Home");
        taskList.addTask("2", "Learn Java", false, "Course");
        taskList.addTask("3", "Learn TDD", false, "Course");
        String actual = taskList.getAllTask();

        assertEquals(expectedTask, actual);
    }

    @Test
    public void testGetTaskById() {
        TaskList taskList = new TaskList();
        String expectedTask = "1. Do dishes [DONE]";

        taskList.addTask("1", "Do dishes", true, "Home");
        String actual = taskList.getTask("1");

        assertEquals(expectedTask, actual);
    }

    @Test
    public void testUpdateTask(){
        TaskList taskList = new TaskList();
        String expectedTask = "2. Learn Java [DONE]";

        taskList.addTask("2", "Learn Java", false, "Course");
        taskList.markTaskDone("2");
        String actual = taskList.getTask("2");

        assertEquals(expectedTask, actual);
    }

    @Test
    public void testAddTask(){
        TaskList taskList = new TaskList();
        String expected = "4. Learn CI/CD [NOT DONE]";

        taskList.addTask("4", "Learn CI/CD", false, "Course");
        String actual = taskList.getTask("4");

        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteTask(){
        TaskList taskList = new TaskList();

        taskList.addTask("4", "Learn CI/CD", false, "Course");
        taskList.deleteTask("4");
        assertNull(taskList.getTask("4"));
    }

    @Test
    public void testGetTaskByCategory(){
        TaskList taskList = new TaskList();
        String expectedTask = "2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]";

        taskList.addTask("1", "Do dishes", true, "Home");
        taskList.addTask("2", "Learn Java", false, "Course");
        taskList.addTask("3", "Learn TDD", false, "Course");
        String actual = taskList.getAllTaskByCategory("Course").toString();

        assertEquals(expectedTask, actual);
    }
}