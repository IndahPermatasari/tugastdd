package com.gojek;

public class Task {
    private String name;
    private boolean status;
    private String id;
    private String category;

    public Task(String id, String name, boolean status, String category) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.category = category;
    }

    public String getId() {
        return this.id;
    }

    public String getCategory() {
        return this.category;
    }

    public void markTaskDone(){
        this.status = true;
    }

    @Override
    public String toString() {
        if(this.status){
            return id + ". " + name + " [DONE]";
        }
        return id + ". " + name + " [NOT DONE]";
    }
}
