package com.gojek;

import java.util.Scanner;

public class Display {
    private Scanner scan = new Scanner(System.in);
    private TaskList taskList = new TaskList();
    private Task task;

    public Display() {
        int input;

        do {
            printMenu();
            input = scan.nextInt();
            scan.nextLine();
            switch (input){
                case 1:
                    System.out.println(taskList.getAllTask());
                    break;
                case 2:
                    taskList.displayUpdateTask();
                    System.out.println(taskList.getAllTask());
                    break;
                case 3:
                    taskList.displayAddTask();
                    System.out.println("Task added!");
                    break;
                case 4:
                    taskList.displayDeleteTask();
                    System.out.println("Task deleted!");
                    break;
                case 5:
                    System.out.println(taskList.displayGetTaskByCategory());
                    break;
                case 6:
                    System.out.println(taskList.displayGetTask());
                    break;
                default:
                    break;
            }
        }while (input != 7);
    }
    private static void printMenu(){
        System.out.println("Task List");
        System.out.println("=======================");
        System.out.println("1. Get all Task List");
        System.out.println("2. Update Task List");
        System.out.println("3. Add Task");
        System.out.println("4. Delete Task");
        System.out.println("5. Get all Task by Category");
        System.out.println("6. Get task by Id");
        System.out.println("7. Exit");
        System.out.println("=======================");
    }
}
