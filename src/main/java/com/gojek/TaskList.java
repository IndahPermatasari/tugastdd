package com.gojek;

import java.util.ArrayList;
import java.util.Scanner;

public class TaskList
{
    private Scanner scan = new Scanner(System.in);
    private ArrayList<Task> taskList;

    public TaskList() {
        taskList = new ArrayList<>();
    }

    public String getAllTask() {
        String allTask = "";
        for (int i = 0; i < taskList.size(); i++) {
            Task task = taskList.get(i);
            allTask = allTask + task.toString();
            if (i != taskList.size() - 1) {
                allTask = allTask + "\n";
            }
        }
        return allTask;
    }

    public String getTask(String id) {
        for(Task task : this.taskList) {
            if(task.getId().equals(id)){
                return task.toString();
            }
        }
        return null;
    }

    public void markTaskDone(String id) {
        for(Task task : this.taskList) {
            if(task.getId().equals(id)){
                task.markTaskDone();
            }
        }
    }

    public void addTask(String id, String name, boolean status, String category) {
        this.taskList.add(new Task(id, name, status, category));
    }

    public void deleteTask(String id) {
        this.taskList.removeIf(e -> e.getId().equals(id));
    }

    public StringBuilder getAllTaskByCategory(String category) {
        StringBuilder allTask = new StringBuilder();
        for(Task task : this.taskList) {
            if(task.getCategory().equals(category)){
                allTask.append(task.toString()).append("\n");
            }
        }
        if(allTask.length() != 0){
            allTask.delete(allTask.length() - 1, allTask.length() + 1);
        }
        return allTask;
    }

    public void displayDeleteTask(){
        System.out.println("Which task you want to delete?");
        String id = scan.nextLine();
        deleteTask(id);
    }

    public void displayUpdateTask(){
        System.out.println("Which task you want to update?");
        String id = scan.nextLine();
        markTaskDone(id);
    }

    public void displayAddTask(){
        System.out.println("Input task id :");
        String id = scan.nextLine();
        System.out.println("Input task name : ");
        String name = scan.nextLine();
        System.out.println("Input task category : ");
        String category = scan.nextLine();

        addTask(id, name, false, category);
    }

    public String displayGetTaskByCategory(){
        System.out.println("Input task category : ");
        String category = scan.nextLine();

        return getAllTaskByCategory(category).toString();
    }

    public String displayGetTask(){
        System.out.println("Input task id : ");
        String id = scan.nextLine();

        return getTask(id);
    }
}
